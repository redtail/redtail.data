using System;

namespace RedTail.Data
{
  public class DataConfiguration
  {
    public static DataSettings Settings { get; private set; } = new DataSettings();

    public static void Configure(Action<DataSettings> cb)
    {
      cb(Settings);
    }
  }
}
