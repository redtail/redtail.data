﻿using System;
using System.Data.SqlClient;
using System.Threading;

namespace RedTail.Data
{
	public class DataHandler<T1, T2, T3>
		: BaseHandler<T1, T2>
		where T1 : DataRequest
		where T2 : DataResponse
		where T3 : DataService
	{
		public DataHandler(T1 request, Boolean stopOnError)
			: base(request, stopOnError)
		{

		}

		public Exception LastError { get; private set; }

		protected override void Command(T1 request)
		{

		}

		protected void DbProcedure(Action<T3> action)
		{
			Boolean complete = false;
			Int32 counter = 5;
			Random random = new Random();
			TimeSpan delay = TimeSpan.FromMilliseconds(random.Next(1000, 5000));
			while (!complete)
			{
				using (T3 db = (T3)Activator.CreateInstance(typeof(T3)))
				{
					try
					{
						action(db);
						complete = true;
					}
					catch (SqlException ex)
					{
						this.LastError = ex;

						--counter;
						if (counter <= 0 || ex.Number != 1205)
							throw;

						Thread.Sleep(delay);
					}
					catch (Exception ex)
					{
						this.LastError = ex;

						throw ex;
					}
				}
			}
		}

		protected void DbTransaction(Action<T3> action)
		{
			this.DbProcedure((T3 db) =>
			{
				try
				{
					db.BeginTransaction();
					action(db);
					db.CommitTransaction();
				}
				catch
				{
					db.RollbackTransaction();
					throw;
				}
			});
		}

		protected override void Done(T2 response)
		{

		}

		protected override void Error(T2 response)
		{

		}
	}
}
