using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace RedTail.Data
{
  public interface IDataService
  {
		SqlConnection Connection { get; }
		SqlTransaction Transaction { get; }

    void BeginTransaction();
    void Close();
    void CommitTransaction();
    DataSet ExecuteCommand(SqlCommand command);
    DataSet ExecuteProcedure(String procedure, List<SqlParameter> parameters);
    DataSet ExecuteText(String text, List<SqlParameter> parameters);
    SqlConnection Open(String connectionString);
    void RollbackTransaction();
  }
}
