using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace RedTail.Data
{
  public class DataService
    : IDataService, IDisposable
  {
    public DataService(String connectionString = null)
		{
			if (connectionString != null)
				this.Open(connectionString);
			else
        this.Open(DataConfiguration.Settings.ConnectionString);
		}

		private Boolean Disposed = false;

		public SqlConnection Connection { get; private set; }
		public SqlTransaction Transaction { get; private set; }

		~DataService()
		{
			this.Dispose(false);
		}

		public void Close()
		{
			if (this.Connection != null && this.Connection.State != ConnectionState.Closed)
				this.Connection.Close();
		}

		public SqlConnection Open(String connectionString)
		{
			// close the previous connection
			this.Close();

			// open the connection
			this.Connection = new SqlConnection(connectionString);
			this.Connection.Open();
			return this.Connection;
		}

		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(Boolean disposing)
		{
			// Check to see if Dispose has already been called.
			if (!this.Disposed)
			{
				// If disposing equals true, dispose all managed.
				if (disposing)
				{

				}

				// close the connection
				this.Close();

				// Dispose unmanaged resources.
				if (this.Transaction != null)
				{
					this.Transaction.Dispose();
					this.Transaction = null;
				}

				if (this.Connection != null)
				{
					this.Connection.Dispose();
					this.Connection = null;
				}

				// Note disposing has been done.
				this.Disposed = true;
			}
		}

		public DataSet ExecuteCommand(SqlCommand command)
		{
			DataSet dataset = new DataSet();
			try
			{
				if (this.Connection != null && command.Connection == null)
					command.Connection = this.Connection;

				if (this.Transaction != null && command.Transaction == null)
					command.Transaction = this.Transaction;

				using (SqlDataAdapter adapter = new SqlDataAdapter())
				{
					adapter.SelectCommand = command;
					adapter.Fill(dataset);
				}

				return dataset;
			}
			catch
			{
				if (dataset != null)
					dataset.Dispose();
				throw;
			}
		}

		public DataSet ExecuteProcedure(String procedure, List<SqlParameter> parameters)
		{
			using (SqlCommand command = new SqlCommand(procedure, this.Connection))
			{
				command.CommandType = CommandType.StoredProcedure;

				foreach (SqlParameter parameter in parameters)
					command.Parameters.Add(parameter);

				return this.ExecuteCommand(command);
			}
		}

		public DataSet ExecuteText(String text, List<SqlParameter> parameters)
		{
			using (SqlCommand command = new SqlCommand(text, this.Connection))
			{
				command.CommandType = CommandType.Text;

				foreach (SqlParameter parameter in parameters)
					command.Parameters.Add(parameter);

				return this.ExecuteCommand(command);
			}
		}

		public void BeginTransaction()
		{
			this.Transaction = this.Connection.BeginTransaction();
		}

		public void CommitTransaction()
		{
			this.Transaction.Commit();
		}

		public void RollbackTransaction()
		{
			this.Transaction.Rollback();
		}
  }
}
