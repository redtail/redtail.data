﻿using System;
using System.Data;
using System.Reflection;

namespace RedTail.Data
{
	public class DataRowParser<T>
		where T : new()
	{
		public DataRowParser(DataRow row, String prefix)
		{
			this.DataRow = row;
			this.Prefix = prefix;
		}

		private readonly DataRow DataRow;
		private readonly String Prefix;

		public T Parse()
		{
			T item = new T();

			foreach (DataColumn itmDataColumn in this.DataRow.Table.Columns)
			{
				this.SetItemPropertyValue(item, itmDataColumn);
			}

			return item;
		}

		private void SetItemPropertyValue(T item, DataColumn dataColumn)
		{
			PropertyInfo propertyInfo = item.GetType().GetProperty(dataColumn.ColumnName);

			if (propertyInfo != null && this.DataRow[dataColumn.ColumnName] != DBNull.Value)
			{
				if (propertyInfo.PropertyType.IsEnum)
				{
					propertyInfo.SetValue(item, Enum.Parse(propertyInfo.PropertyType, Convert.ToString(this.DataRow[dataColumn.ColumnName])), null);
				}
				else
				{
					propertyInfo.SetValue(item, this.DataRow[dataColumn.ColumnName], null);
				}
			}
		}
	}
}
