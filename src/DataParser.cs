﻿using System;
using System.Data;
using System.Reflection;

namespace RedTail.Data
{
	public class DataParser<T>
		where T : new()
	{
		public DataParser()
		{

		}

		public T Parse(DataRow row)
		{
			T item = new T();

			foreach (DataColumn itmDataColumn in row.Table.Columns)
			{
				this.SetItemPropertyValue(row, itmDataColumn, item);
			}

			return item;
		}

		private void SetItemPropertyValue(DataRow row, DataColumn dataColumn, T item)
		{
			PropertyInfo propertyInfo = item.GetType().GetProperty(dataColumn.ColumnName);

			if (propertyInfo != null && row[dataColumn.ColumnName] != DBNull.Value)
			{
				if (propertyInfo.PropertyType.IsEnum)
				{
					propertyInfo.SetValue(item, Enum.Parse(propertyInfo.PropertyType, Convert.ToString(row[dataColumn.ColumnName])), null);
				}
				else
				{
					propertyInfo.SetValue(item, row[dataColumn.ColumnName], null);
				}
			}
		}
	}
}
