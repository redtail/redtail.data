using System;

namespace RedTail.Data
{
  public class DataSettings
  {
    public String ConnectionString { get; set; }
  }
}
